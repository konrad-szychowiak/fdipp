# Fizyczne Kodowanko
> Wariant B

## Materiały
* Google [drive] (oba warianty)


## Polecenie
+ Zbadaj jak zależy wartość średniej drogi swobodnej od liczby atomów, tj. `λ(N)` przy
stałej wartości `M`.

+ Wykreśl badaną zależności dla różnych wartości `M`.

+ Zbadaj jak zależy częstość zderzeń `n` cząstki czerwonej od liczby atomów, tj. `n(N)`
przy stałej wartości `M`.

+ Wykreśl badaną zależności dla różnych wartości `M`.


## Raport
Przedstaw raport końcowy eksperymentu numerycznego:

- skład zespołu realizującego projekt,
- wykaz prac wykonanych przez poszczególnych członków zespołu:
- procentowy udział w realizacji projektu poszczególnych członków zespołu
- wykaz przesłanych plików,
- wykaz zapożyczonych bibliotek ( wskazać źródło) ,
- 2-4 opisane zrzuty ekranowe ilustrujące działanie programu,
- opisane wykresy przedstawiające wyznaczone zależności λ̅(N)i n(N).

<!-- links -->
[drive]: https://drive.google.com/drive/folders/1IoM1tIQTbqbW0BJGsf5OCD86Oklc-4ye
