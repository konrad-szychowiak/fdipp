from fib.gui import get_data
from fib.atom import Atom
from fib.simulation import simulation

if __name__ == '__main__':
    simulation(*get_data())
