"""
This file boxtains simulation of the atoms behavior
"""
import math
import random as rand
from .atom import Atom
from .vector import Vector
from .container import Container
from .gui import show_results
from .generator import generator, inject_particles
from .visualisation import create_visalisation, update_visualisation, HideVisualisation

atom_radius = 1
container_size = 20 * atom_radius
particles_quantity = 5  # MAX IS 0.15*(boxtainer_H*boxtainer_W)
time_in_s = 10


def simulation(n_particles: int, m_time: int, container_size: int, atom_radius: int, V: int):
    show_visualisation = True

    K = math.ceil(container_size * 0.1)  # κ ≥ min(ηH, ηL)

    time_step = 1 / (K * V)
    box = Container(container_size, atom_radius)

    # generating data
    data = generator(n_particles, container_size,
                     container_size, atom_radius, V)
    particles_quantity = len(data)

    # The Red Particle data
    quantity_of_collisions = 0
    distance_traveled = 0

    particles = inject_particles(data)

    print(f"Container: {box.inner_size} {box.inner_size}")
    print(f"time pos box_maxes")

    visualisation_controller = create_visalisation(particles, container_size)

    for i in range(m_time * int(time_step**(-1))):
        # OVERLAP DETECTON
        for a in range(n_particles):
            for b in range(a + 1, n_particles):
                if math.sqrt((particles[a].next(time_step).x - particles[b].next(time_step).x) ** 2 + (particles[a].next(time_step).y - particles[b].next(time_step).y) ** 2) <= 2 * particles[a].r:
                    particles[a].collide(particles[b])
                    if a == 0 or b == 0:
                        quantity_of_collisions += 1
                    # print("WARNING! BALL INJECTION")

        # WALL DETECTION
        for j in range(particles_quantity):

            if particles[j].next(time_step).y > box.inner_size or particles[j].next(time_step).y < 0:
                particles[j].velocity.y = -particles[j].velocity.y

            if particles[j].next(time_step).x > box.inner_size or particles[j].next(time_step).x < 0:
                particles[j].velocity.x = -particles[j].velocity.x

            if j == 0:
                red = (particles[j].position.x, particles[j].position.y)
                distance_traveled += math.sqrt((red[0] - particles[j].next(
                    time_step).x)**2 + (red[1] - particles[j].next(time_step).y)**2)
            # UPDATE
            particles[j].update(time_step)
            i += 1

        try:
            if show_visualisation:
                update_visualisation(particles, *visualisation_controller)
        except AttributeError:
            break
        except HideVisualisation:
            show_visualisation = False

    show_results(distance_traveled, quantity_of_collisions)

    print(f"***************RED DATA***************")
    print(f"Distance traveled:    {distance_traveled}")
    print(f"Quantity of traveled: {quantity_of_collisions}")
    print(
        "Average distance:     {0}".format(distance_traveled / quantity_of_collisions if quantity_of_collisions != 0 else distance_traveled))


if __name__ == '__main__':
    simulation(particles_quantity, time_in_s, container_size, atom_radius)
