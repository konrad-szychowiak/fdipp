"""
This file contains a class
for container containing atoms in the simulation
"""

from .vector import Vector


class Container:
    def __init__(self, size: float, atom_radius: float = 1.0):
        self.size = size
        self.r = atom_radius

    @property
    def width(self):
        return self.size

    @property
    def height(self):
        return self.size

    @property
    def inner_size(self):
        return self.size - 2 * self.r

    def breakaway(self, position: Vector) -> list:
        x, y = 1, 1
        if 0 > position.x or position.x > self.inner_size:
            print("out-of-container: HORIZONTAL (x) WALL BUMP")
            x = -1

        if 0 > position.y or position.y > self.inner_size:
            print("out-of-container: VERTICAL (y) WALL BUMP")
            y = -1

        return x, y
