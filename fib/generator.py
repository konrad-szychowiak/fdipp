"""
This file contains generator for atoms
"""
import random as rand
import math
from .atom import Atom
from .vector import Vector

def inject_particles(prototypes: [float, float, float, float, float]) -> [Atom]:
    """
    Function transforms atoms' prototypes from generator
    into real Atom objects; listed
    """
    particles = []
    # OVERLAP DETECTON
    for prot in prototypes:
        particle_position = Vector(prot[0], prot[1])
        particle_velocity = Vector(prot[2], prot[3])
        particles.append(Atom(particle_position, particle_velocity, prot[4]))
    return particles

def generator(particles_quantity: float, container_H: float, container_W: float, atom_radius: float, V: float):
    result = list()
    overlap = False

    result.append([0, 0, rand.randrange(0, V), rand.randrange(0, V), atom_radius])

    if particles_quantity > (0.15 * (container_H * container_W)):
        particles_quantity = int(0.15 * (container_H * container_W))
    print(particles_quantity)

    while(particles_quantity - 1 > 0):
        x = rand.randrange(0, container_W + 1 - 2 * atom_radius)
        y = rand.randrange(0, container_H + 1 - 2 * atom_radius)
        v_x = x + rand.randrange(-V, V)
        v_y = y + rand.randrange(-V, V)
        overlap = False

        if not result:
            result.append([x, y, v_x, v_y, atom_radius])
            particles_quantity -= 1

        else:
            for i in range(len(result)):
                if math.sqrt((result[i][0] - x) ** 2 + (result[i][1] - y) ** 2) < 2 * atom_radius:
                    overlap = True
                    break

            if not overlap:
                result.append([x, y, v_x, v_y, atom_radius])
                particles_quantity -= 1
    return result
