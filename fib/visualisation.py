""" Wizualizacja ruchu atomów """
# Do działania potrzebuje: Wymiarów kontenera, listy atomów

from turtle import *
import PySimpleGUI as sg
# import math
from .atom import Atom
from .container import Container
# from .generator import Generator
from .vector import Vector
from .simulation import inject_particles

import random


class HideVisualisation(Exception):
    def __init__(self, *args, **kwargs):
        super(HideVisualisation, self).__init__(*args, **kwargs)


def turtle_position(container_size, atom) -> [float, float]:
    return [(atom.position.x - 0.5 * container_size + atom.r)
            * 10, (atom.position.y - 0.5 * container_size + atom.r) * 10]


def create_visalisation(particles: [Atom], container_size: int) -> [[RawTurtle], sg.Window]:
    # TODO: CONTAINER_SIZE
    # CREATE WINDOW
    layout = [[sg.Canvas(size=(container_size * 10, container_size * 10),
                         key='-canvas-')], [sg.Button('STOP', key='_STOP_'), sg.Button('Fast Forward', key='_HIDE_')], ]

    window = sg.Window('Symulacja atomów', layout, finalize=True)

    canvas = window['-canvas-'].TKCanvas

    # LIST OF ATOMS' GRAPHICS REPRESENTATION
    vis_particles = list()
    for p in particles:
        vis_p = RawTurtle(canvas)
        vis_p.penup()
        vis_p.speed(0)
        vis_p.shape("circle")
        vis_p.setpos((p.position.x - 0.5 * container_size + p.r) * 10,
                     (p.position.y - 0.5 * container_size + p.r) * 10)
        vis_p.shapesize(p.r)
        vis_p.color('blue')
        vis_particles.append(vis_p)

    vis_particles[0].color('red')
    return vis_particles, container_size, window


def update_visualisation(particles, vis_particles, container_size, window):
    event, _ = window.read(timeout=10)

    if event in ('_HIDE_'):
        raise HideVisualisation

    if event in (sg.WIN_CLOSED, '_STOP_', None):
        print(event)
        window.close()
        return 1

    for k in range(len(particles)):
        vis_particles[k].setpos(
            *turtle_position(container_size, particles[k]))
    return 0
