"""
This module handles data input from GUI
"""

import PySimpleGUI as sg
# from math import round


def get_data() -> [int, int, int, int, int]:
    layout = [[sg.Text('Podaj ilość atomów: \t\t'), sg.InputText(2)],
              [sg.Text('Podaj ilość jednostek czasu: \t'), sg.InputText(10)],
              [sg.Text('Podaj długość boku zbiornika:\t'), sg.InputText(50)],
              [sg.Text('Podaj promień atomu: \t\t'), sg.InputText(1)],
              [sg.Text('Podaj zakres wartości prędkości: \t'),
               sg.InputText(50)],
              [sg.Submit('OK')]]

    window = sg.Window('Symulacja atomów', layout)

    event, values = window.read()
    window.close()

    n, M, a, r, v = [int(values[x]) for x in values.keys()]

    return n, M, a, r, v


def show_results(distance_traveled, quantity_of_collisions):
    layout = [[sg.Text('Przebyty dystans: \t\t'), sg.Text(f'{distance_traveled:.2f}')],
              [sg.Text('Liczba zderzeń: \t\t'), sg.Text(
                  f'{quantity_of_collisions}')],
              [sg.Text('Średnia droga swobodna: \t'), sg.Text(
                  f'{(distance_traveled/quantity_of_collisions if quantity_of_collisions != 0 else distance_traveled):.2f}')],
              [sg.Submit('OK')]]

    window = sg.Window('Podsumowanie', layout)

    window.read()
    window.close()
