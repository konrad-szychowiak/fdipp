from math import cos, acos, sin, asin, tan, atan, sqrt


class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @property
    def magnitude(self):
        return sqrt(self.x * self.x + self.y * self.y)

    @property
    def normalized(self):
        return Vector(self.x / self.magnitude, self.y / self.magnitude)

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def dot(self, other):
        return self.x * other.x + self.y * other.y

    def scale(self, other):
        return Vector(self.x * other.x, self.y * other.y)

    def __mul__(self, other):
        return Vector(self.x * other, self.y * other)

    def __rmul__(self, other):
        return Vector(self.x * other, self.y * other)

    @property
    def perpendicular(self):
        return Vector(-self.y, self.x)

    def angle(self, other):
        return acos(self.dot(other) / (self.magnitude * other.magnitude))

    def __str__(self):
        return "({0},{1})".format(self.x, self.y)
