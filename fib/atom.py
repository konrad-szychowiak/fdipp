"""
This file contains representations of atoms
and utilities for them
"""

from math import cos, acos, sin, asin, tan, atan, sqrt
from .vector import Vector


class Atom:
    def __init__(self, position: Vector, velocity: Vector, radius: float = 1.0):
        self.position = position
        self.velocity = velocity
        self.r = radius

    def __str__(self):
        return "(Atom {0},{1})".format(self.position, self.velocity)

    def update(self, time, position: [float, float] = None) -> object:
        # if position:
        #     self.x, self.y = position
        # else:
        self.position = self.next(time)
        return self

    def mass(self, newMass: float):
        if newMass:
            self.m = newMass
        return self.m

    def next(self, time=1):
        return self.position + self.velocity * time

    def collide(self, other):
        pararell = (self.position - other.position).normalized
        perpendicular = pararell.perpendicular
        x1 = cos(self.velocity.angle(pararell)) * \
            self.velocity.magnitude * pararell
        y1 = cos(self.velocity.angle(perpendicular)) * \
            self.velocity.magnitude * perpendicular
        x2 = cos(other.velocity.angle(pararell)) * \
            other.velocity.magnitude * pararell
        y2 = cos(other.velocity.angle(perpendicular)) * \
            other.velocity.magnitude * perpendicular
        self.velocity = x2 + y1
        other.velocity = x1 + y2
        return other

    def deflect(self, deflector: [int, int]) -> object:
        self.velocity.scale(Vector(*deflector))
        return self
